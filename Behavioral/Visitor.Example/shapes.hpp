#pragma once

#include "shape_visitor.hpp"
#include <iostream>

class Shape
{
public:
    virtual void draw() const = 0;
    virtual void accept(ShapeVisitor& visitor) = 0;
    virtual ~Shape() = default;
};

// CRTP
template <typename ShapeToVisit>
struct VisitableShape : public Shape
{
    void accept(ShapeVisitor& visitor)
    {
        visitor.visit(static_cast<ShapeToVisit&>(*this));
    }
};

class Square : public Shape
{
	
};

class Circle : public Shape
{
public:
    Circle(int radius) : radius_(radius) {}

    void draw() const override
    {
        std::cout << "Circle - r: " << radius_ << std::endl;
    }

    int radius() const
    {
        return radius_;
    }

    void accept(ShapeVisitor& visitor) override
    {
        visitor.visit(*this);
    }

private:
    int radius_;
};

class Rectangle : public VisitableShape<Rectangle>
{
public:
    Rectangle(int width, int height) : width_{width}, height_{height} {}

    void draw() const override
    {
        std::cout << "Rectangle - w: " << width_ << "; h: " << height_ << std::endl;
    }

    int width() const
    {
        return width_;
    }

    int height() const
    {
        return height_;
    }

private:
    int width_, height_;
};

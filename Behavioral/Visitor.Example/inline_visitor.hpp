#ifndef INLINE_VISITOR_HPP
#define INLINE_VISITOR_HPP

#include <utility>
#include <cstddef>

namespace InlineVisitor
{
    template <typename T, typename F, typename BaseInner, typename Args>
    struct ComposeVisitor
    {
        struct Inner : public BaseInner
        {
            using BaseInner::visit;

            Inner(Args&& args) : BaseInner{ std::move(args.second) }, func_{ std::move(args.first) } {}

            virtual void visit(T& t) final override
            {
                func_(t);
            }

        private:
            F func_;
        };

        ComposeVisitor(Args&& args) : args_{ std::move(args) } {}

        template <typename TAdd, typename FAdd>
        ComposeVisitor<TAdd, FAdd, Inner, std::pair<FAdd, Args>> on(FAdd&& f)
        {
            return ComposeVisitor<TAdd, FAdd, Inner, std::pair<FAdd, Args>>{ std::make_pair(std::forward<FAdd>(f), std::move(args_)) };
        }

        Inner end_visitor()
        {
            return Inner{ std::move(args_) };
        }

    private:
        Args args_;
    };

    template <typename VisitorBase>
    struct EmptyVisitor
    {
        struct Inner : public VisitorBase
        {
            using VisitorBase::visit;

            Inner(std::nullptr_t) {}
        };

        template <typename TAdd, typename FAdd>
        ComposeVisitor<TAdd, FAdd, Inner, std::pair<FAdd, std::nullptr_t>> on(FAdd&& f)
        {
            return ComposeVisitor<TAdd, FAdd, Inner, std::pair<FAdd, std::nullptr_t>>{ std::make_pair(std::forward<FAdd>(f), nullptr) };
        }
    };

    template <typename VisitorBase>
    EmptyVisitor<VisitorBase> begin_visitor()
    {
        return EmptyVisitor<VisitorBase>();
    }
}

#endif

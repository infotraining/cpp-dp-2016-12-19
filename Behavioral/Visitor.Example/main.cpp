#include <iostream>
#include <vector>
#include <memory>
#include "inline_visitor.hpp"
#include "shapes.hpp"
#include "shape_visitor.hpp"

using namespace std;
using namespace InlineVisitor;

using ShapePtr = shared_ptr<Shape>;

int main()
{
	vector<ShapePtr> shapes;
	shapes.push_back(make_shared<Circle>(1));
    shapes.push_back(make_shared<Rectangle>(2, 4));

	auto area = 0.0;

    auto v = begin_visitor<ShapeVisitor>()
        .on<Circle>([&area](Circle& c) {
            area += c.radius() * c.radius() * 3.14;
        })
        .on<Rectangle>([&area](Rectangle& r) {
            area += r.width() * r.height();
        })
		.on<Square>([&area](Square& s)
		{
			area += 1.0;
		})
        .end_visitor();

    for(const auto& shape : shapes)
    {
        shape->accept(v);
    }

	std::cout << "Area: " << area << endl;
}

#include "stock.hpp"

using namespace std;

int main()
{
	Stock misys("Misys", 340.0);
	Stock ibm("IBM", 245.0);
	Stock tpsa("TPSA", 95.0);

	// rejestracja inwestor�w zainteresowanych powiadomieniami o zmianach kursu sp�ek
	Investor kulczyk_jr{ "Kulczyk Jr" };
	auto solorz = make_shared<Investor>("Solorz");

	misys.attach([&kulczyk_jr](auto s, auto p) { kulczyk_jr.update_portfolio(s, p); });
	ibm.attach([&kulczyk_jr](auto s, auto p) { kulczyk_jr.update_portfolio(s, p); });
	auto conn_ibm_solorz_jr = ibm.attach(
		[solorz](auto s, auto p) { solorz->update_portfolio(s, p); });

	solorz.reset();

	// zmian kurs�w
	misys.set_price(360.0);
	ibm.set_price(210.0);
	tpsa.set_price(45.0);

	conn_ibm_solorz_jr.disconnect();

	cout << "\n\n" << endl;

	misys.set_price(380.0);
	ibm.set_price(230.0);
	tpsa.set_price(15.0);
}

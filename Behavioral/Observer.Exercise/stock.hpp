#ifndef STOCK_HPP_
#define STOCK_HPP_

#include <string>
#include <iostream>
#include <boost/signals2.hpp>

// Subject
class Stock
{
	using StockPriceChangedSignal = boost::signals2::signal<void(std::string, double)>;
	using StockPriceChangedSlot = typename StockPriceChangedSignal::slot_type;

private:
	std::string symbol_;
	double price_;
	StockPriceChangedSignal price_changed_;

public:
	Stock(const std::string& symbol, double price) : symbol_(symbol), price_(price)
	{

	}

	std::string get_symbol() const
	{
		return symbol_;
	}

	double get_price() const
	{
		return price_;
	}

	boost::signals2::connection attach(const StockPriceChangedSlot& observer)
	{
		return price_changed_.connect(observer);
	}

	void set_price(double price)
	{
		if (price != price_)
		{
			price_ = price;
			price_changed_(symbol_, price_);
		}		
	}
};

class Investor 
{
	std::string name_;
public:
	Investor(const std::string& name) : name_(name)
	{
	}

	~Investor()
	{
		std::cout << name_ << " destroyed..." << std::endl;
	}

	void update_portfolio(std::string symbol, double price)
	{
		std::cout << name_ << " is notified: " << symbol << " - " << price << "$" << std::endl;
	}
};

#endif /*STOCK_HPP_*/

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <numeric>
#include <fstream>
#include <iterator>
#include <list>
#include <stdexcept>
#include <memory>

struct StatResult
{
	std::string description;
	double value;

	StatResult(const std::string& desc, double val) : description(desc), value(val)
	{
	}
};

using Data = std::vector<double>;
using Results = std::vector<StatResult>;

class Statistics
{
public:
	virtual Results calculate(const Data& data) = 0;
	virtual ~Statistics() = default;
};

class Avg : public Statistics
{
public:
	Results calculate(const Data& data) override
	{
		

		double sum = std::accumulate(data.begin(), data.end(), 0.0);
		double avg = sum / data.size();

		Results results;
		StatResult result("AVG", avg);
		results.push_back(result);

		return results;
	}
};

class MinMax : public Statistics
{
public:
	Results calculate(const Data& data) override
	{
		double min = *(std::min_element(data.begin(), data.end()));
		double max = *(std::max_element(data.begin(), data.end()));

		Results results;
		results.push_back(StatResult("MIN", min));
		results.push_back(StatResult("MAX", max));
		return results;
	}
};

class Sum : public Statistics
{
public:
	Results calculate(const Data& data) override
	{
		double sum = std::accumulate(data.begin(), data.end(), 0.0);

		Results results;
		results.push_back(StatResult("SUM", sum));

		return results;
	}
};

using StatisticsPtr = std::shared_ptr<Statistics>;

class StatGroup : public Statistics
{
	std::vector<StatisticsPtr> stats_;
public:
	Results calculate(const Data& data) override
	{
		Results results;

		for(const auto& stat : stats_)
		{
			auto calc_result = stat->calculate(data);

			results.insert(results.end(),
				std::make_move_iterator(calc_result.begin()), std::make_move_iterator(calc_result.end()));
		}

		return results;
	}

	void add(StatisticsPtr stat)
	{
		stats_.emplace_back(stat);
	}
};

class DataAnalyzer
{
	StatisticsPtr statistics_;
	Data data_;
	Results results_;
public:
	DataAnalyzer(StatisticsPtr statistics) : statistics_ {statistics}
	{
	}

	void load_data(const std::string& file_name)
	{
		results_.clear();

		data_ = do_load_data(file_name);
	}

	void set_statistics(StatisticsPtr stat_type)
	{
		statistics_ = stat_type;
	}

	void calculate()
	{
		auto calc_result = statistics_->calculate(data_);

		results_.insert(results_.end(),
			std::make_move_iterator(calc_result.begin()), std::make_move_iterator(calc_result.end()));
	}

	const Results& results() const
	{
		return results_;
	}
protected:
	virtual Data do_load_data(const std::string& file_name)
	{
		Data data;

		std::ifstream fin(file_name.c_str());
		if (!fin)
			throw std::runtime_error("File not opened");

		double d;
		while (fin >> d)
		{
			data.push_back(d);
		}

		std::cout << "File " << file_name << " has been loaded...\n";

		return data;
	}
};

void show_results(const Results& results)
{
    for(const auto& rslt : results)
		std::cout << rslt.description << " = " << rslt.value << std::endl;
}

int main()
{
	using namespace std;

	auto avg = make_shared<Avg>();
	auto min_max = make_shared<MinMax>();
	auto sum = make_shared<Sum>();
	auto std_stats = make_shared<StatGroup>();
	std_stats->add(avg);
	std_stats->add(min_max);
	std_stats->add(sum);

	DataAnalyzer da {std_stats};
	da.load_data("data.dat");
	da.calculate();

	show_results(da.results());

	std::cout << "\n\n";

	da.load_data("new_data.dat");
	da.calculate();

	show_results(da.results());
}

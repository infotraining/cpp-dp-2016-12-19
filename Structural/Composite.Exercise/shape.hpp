#ifndef SHAPE_HPP
#define SHAPE_HPP

#include <memory>
#include "point.hpp"

namespace Drawing
{
	//struct A
	//{
	//	template <typename T,
	//			  typename Arg = std::enable_if_t<!std::is_same<std::decay_t<T>, A>::value>>
	//	A(T&& a)
	//	{}

	//	A(const A& a) //cc
	//	{}
	//};

	//{
	//	A a;
	//	A b(a);
	//}

    class Shape
    {
    public:
        virtual ~Shape() = default;
        virtual void move(int x, int y) = 0;
        virtual void draw() const = 0;
        virtual std::unique_ptr<Shape> clone() const = 0;
    };

    template <typename BaseType, typename DerivedType = Shape>
    class CloneableShape : public DerivedType
    {
    public:
		using DerivedType::DerivedType;

        std::unique_ptr<Shape> clone() const override
        {
            return std::make_unique<BaseType>(static_cast<const BaseType&>(*this));
        }
    };

    class ShapeBase : public Shape
    {
        Point coord_; // composition
    public:
        Point coord() const
        {
            return coord_;
        }

        void set_coord(const Point& pt)
        {
            coord_ = pt;
        }


        ShapeBase(int x = 0, int y = 0)
            : coord_{x, y}
        {}

        void move(int dx, int dy) override
        {
            coord_.translate(dx, dy);
        }
    };
}

#endif // SHAPE_HPP

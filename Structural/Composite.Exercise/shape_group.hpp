#ifndef SHAPEGROUP_HPP
#define SHAPEGROUP_HPP

#include <vector>
#include <memory>

#include "shape.hpp"

namespace Drawing
{
	using ShapePtr = std::unique_ptr<Shape>;

    class ShapeGroup : public CloneableShape<ShapeGroup>
    {
		std::vector<ShapePtr> shapes_;

    public:
		static constexpr const char* id = "ShapeGroup";

		ShapeGroup() = default;

	    ShapeGroup(const ShapeGroup& other)	     
	    {
			for (const auto& shp : other.shapes_)
				shapes_.push_back(shp->clone());
	    }

	    ShapeGroup& operator=(const ShapeGroup& other)
	    {
			ShapeGroup temp(other);
			swap(temp);

			return *this;
	    }

		ShapeGroup(ShapeGroup&& other) noexcept = default;
		ShapeGroup& operator=(ShapeGroup&& other) noexcept = default;

	    void move(int dx, int dy) override
		{
			for (const auto& shp : shapes_)
				shp->move(dx, dy);
		}

	    void draw() const override
		{
			for (const auto& shp : shapes_)
				shp->draw();
		}

		void add(ShapePtr shp)
	    {
			shapes_.push_back(std::move(shp));
	    }

		void swap(ShapeGroup& other) noexcept(noexcept(std::swap(shapes_, other.shapes_)))
	    {
			std::swap(shapes_, other.shapes_);
			shapes_.swap(other.shapes_);
	    }
    };
}


#endif // SHAPEGROUP_HPP

#include "shape_group_reader_writer.hpp"
#include "../shape_group.hpp"
#include "../shape_factories.hpp"

using namespace std;
using namespace Drawing;
using namespace Drawing::IO;

namespace
{
	bool is_registered =
		SingletonShapeRWFactory::instance()
			.register_creator(type_index{ typeid(ShapeGroup) },
				[] { return make_unique<ShapeGroupReaderWriter>(SingletonShapeFactory::instance(), SingletonShapeRWFactory::instance()); });
}

void ShapeGroupReaderWriter::read(Shape& shp, std::istream& in)
{
	ShapeGroup& sg = static_cast<ShapeGroup&>(shp);

	int count;
	in >> count;

	for(int i = 0; i < count; ++i)
	{
		string shape_id;
		
		in >> shape_id;
		
		cout << "Loading " << shape_id << "..." << endl;

		auto shape = shape_factory_.create(shape_id);
		auto shape_rw = shape_rw_factory_.create(create_type_index(*shape));

		shape_rw->read(*shape, in);

		sg.add(move(shape));
	}
}

void ShapeGroupReaderWriter::write(Shape& shp, std::ostream& out)
{
}

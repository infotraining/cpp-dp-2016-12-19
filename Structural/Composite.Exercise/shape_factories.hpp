#ifndef SHAPE_FACTORIES_HPP
#define SHAPE_FACTORIES_HPP

#include <typeindex>
#include "singleton.hpp"
#include "generic_factory.hpp"
#include "shape.hpp"
#include "shape_readers_writers/shape_reader_writer.hpp"

namespace Drawing
{
    using ShapeFactory = GenericFactory<Drawing::Shape>;
    using SingletonShapeFactory = SingletonHolder<ShapeFactory>;

    using ShapeRWFactory = GenericFactory<Drawing::IO::ShapeReaderWriter, std::type_index>;
    using SingletonShapeRWFactory = SingletonHolder<ShapeRWFactory>;

	inline std::type_index create_type_index(Shape& shape)
	{
		return std::type_index(typeid(shape));
	}
}

#endif // SHAPE_FACTORIES_HPP

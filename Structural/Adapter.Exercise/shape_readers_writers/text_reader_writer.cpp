#include "text_reader_writer.hpp"
#include "../text.hpp"
#include "../shape_factories.hpp"

using namespace std;
using namespace Drawing;
using namespace IO;

// TODO - rejestracja fabeyki dla TextReaderWriter
namespace
{
	bool is_registered =
		SingletonShapeRWFactory::instance()
			.register_creator(make_type_index<Text>(), &make_unique<TextReaderWriter>);
}

void TextReaderWriter::read(Shape& shp, istream& in)
{
	Text& paragraph = static_cast<Text&>(shp);

	Point pt;
	string txt;

	in >> pt >> txt;

	paragraph.set_coord(pt);
	paragraph.set_text(txt);
}

void TextReaderWriter::write(Shape& shp, ostream& out)
{
	Text& paragraph = static_cast<Text&>(shp);

	out << Text::id << " "
		<< paragraph.coord() << " "
		<< paragraph.text() << endl;
}
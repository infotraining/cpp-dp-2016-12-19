#ifndef TEXT_READER_WRITER_HPP
#define TEXT_READER_WRITER_HPP

#include "shape_reader_writer.hpp"
#include "../text.hpp"

namespace Drawing
{
    namespace IO
    {
        class TextReaderWriter : public ShapeReaderWriter
        {
            // ShapeReaderWriter interface
        public:
            void read(Shape& shp, std::istream& in) override;

            void write(Shape& shp, std::ostream& out) override;
        };
    }
}
#endif // TEXT_READER_WRITER_HPP

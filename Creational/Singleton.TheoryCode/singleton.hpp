#ifndef SINGLETON_HPP_
#define SINGLETON_HPP_

#include <memory>
#include <iostream>
#include <mutex>
#include <atomic>

//class DoubleCheckedLockingPattern
//{
//	static std::atomic<DoubleCheckedLockingPattern*> instance_;
//	static std::mutex mtx_;
//
//	DoubleCheckedLockingPattern& instance()
//	{
//		if (!instance_.load())
//		{
//			std::lock_guard<std::mutex> lk{ mtx_ };
//			if (!instance_.load())
//			{
//				instance_ = new DoubleCheckedLockingPattern();
//
//				// 1 - alloc
//				void* raw_mem = ::operator new(sizeof(DoubleCheckedLockingPattern));
//				// 2 - placement new
//				new (raw_mem) DoubleCheckedLockingPattern();
//				// 3 - pointer assign
//				instance_.store(static_cast<DoubleCheckedLockingPattern*>(raw_mem));				
//			}
//		}
//
//		return *instance_;
//	}
//};
//
//std::atomic<DoubleCheckedLockingPattern*> DoubleCheckedLockingPattern::instance_;
//std::mutex DoubleCheckedLockingPattern::mtx_;

namespace SinceVS2015
{
	class Singleton
	{
	public:
		Singleton(const Singleton&) = delete;
		Singleton& operator=(const Singleton&) = delete;

		static Singleton& instance()
		{
			static Singleton unique_instance;
			return unique_instance;
		}

		void do_something();

	private:
		Singleton() // disallows creation of new instances outside the class
		{
			std::cout << "Constructor of singleton" << std::endl;
		}

		~Singleton()
		{
			std::cout << "Singleton has been destroyed!" << std::endl;
		}
	};

	void Singleton::do_something()
	{
		std::cout << "Singleton instance at " << std::hex << &instance() << std::endl;
	}
}

class Singleton
{
public:
    Singleton(const Singleton&) = delete;
    Singleton& operator=(const Singleton&) = delete;

	static Singleton& instance()
	{
		std::call_once(init_flag_, [] { instance_.reset(new Singleton()); });

		return *instance_;
	}

	void do_something();
	using DeleterType = typename std::unique_ptr<Singleton>::deleter_type;
	friend struct DeleterType;

private:
	static std::unique_ptr<Singleton> instance_;  // uniqueInstance
	static std::once_flag init_flag_;
	
	Singleton() // disallows creation of new instances outside the class
	{ 
		std::cout << "Constructor of singleton" << std::endl; 
	} 

	~Singleton()
	{
		std::cout << "Singleton has been destroyed!" << std::endl;
	}
};

std::unique_ptr<Singleton> Singleton::instance_;
std::once_flag Singleton::init_flag_;

void Singleton::do_something()
{
	std::cout << "Singleton instance at " << std::hex << &instance() << std::endl;
}


class Service
{
public:
	void run()
	{
		get_logger().log("Start");
	}

protected:
	virtual Log& get_logger()
	{
		return Logger::instance();
	}
};

#endif /*SINGLETON_HPP_*/

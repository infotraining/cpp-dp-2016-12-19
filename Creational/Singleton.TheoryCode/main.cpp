#include <iostream>
#include "singleton.hpp"

using namespace std;

int main()
{
	SinceVS2015::Singleton::instance().do_something();

	SinceVS2015::Singleton& singleObject = SinceVS2015::Singleton::instance();
	singleObject.do_something();
}

#include <iostream>
#include <vector>
#include <functional>
#include <algorithm>
#include <memory>
#include <string>

using namespace std;

#define MOTIF

enum class IconType
{
    none, ok , cancel, warning, error
};

class Widget
{
public:
    virtual void draw() = 0;
    virtual ~Widget() = default;
};

class Button : public Widget
{
    std::string caption_;
    IconType icon_type_;
public:
    Button(const std::string& caption, IconType icon_type) : caption_{caption}, icon_type_{icon_type}
    {}

    std::string caption() const
    {
        return caption_;
    }

    IconType icon() const
    {
        return icon_type_;
    }
};

class Menu : public Widget
{
    std::string text_;
public:
    Menu(const std::string& text) : text_{text}
    {}

    std::string text() const
    {
        return text_;
    }
};


class MotifButton : public Button
{        
public:   
    using Button::Button;

    void draw() override
    {
        cout << "MotifButton [ " << caption() << " ]\n";
    }
};

class MotifMenu : public Menu
{
public:
    using Menu::Menu;

    void draw() override
    {
        cout << "MotifMenu { " << text() << " }\n";
    }
};

class WindowsButton : public Button
{
public:
    using Button::Button;

    void draw() override
    {
        cout << "WindowsButton [ " << caption() << " ]\n";
    }
};

class WindowsMenu : public Menu
{
public:
    using Menu::Menu;

    void draw() override
    {
        cout << "WindowsMenu { " << text() << " }\n";
    }
};

class WidgetFactory
{
public:
	virtual std::unique_ptr<Button> create_button(const std::string& text, IconType icon) = 0;
	virtual std::unique_ptr<Menu> create_menu(const std::string& caption) = 0;
	virtual ~WidgetFactory() = default;
};

class WindowsWidgetFactory : public WidgetFactory
{
public:
	std::unique_ptr<Button> create_button(const std::string& text, IconType icon) override
	{
		return std::make_unique<WindowsButton>(text, icon);
	}

	std::unique_ptr<Menu> create_menu(const std::string& caption) override
	{
		return std::make_unique<WindowsMenu>(caption);
	}
};

class MotifWidgetFactory : public WidgetFactory
{
public:
	std::unique_ptr<Button> create_button(const std::string& text, IconType icon) override
	{
		return std::make_unique<MotifButton>(text, icon);
	}

	std::unique_ptr<Menu> create_menu(const std::string& caption) override
	{
		return std::make_unique<MotifMenu>(caption);
	}
};

#ifdef MOTIF
using AppWidgetFactory = MotifWidgetFactory;
#else
using AppWidgetFactory = WindowsWidgetFactory;
#endif

class Window
{
	std::vector<std::unique_ptr<Widget>> widgets;
public:
	void display() const
	{
		std::cout << "######################\n";
		for (const auto& w : widgets)
			w->draw();
		std::cout << "######################\n\n";
	}

	void add_widget(std::unique_ptr<Widget> widget)
	{
		widgets.push_back(move(widget));
	}
protected:
	static WidgetFactory& get_widget_factory()
	{	
		return widget_factory_;
	}

	static AppWidgetFactory widget_factory_;
};

AppWidgetFactory Window::widget_factory_;

class WindowOne : public Window
{
public:
    WindowOne()
    {		
		add_widget(get_widget_factory().create_button("OK", IconType::ok));
		add_widget(get_widget_factory().create_menu("File"));
    }
};

class WindowTwo : public Window
{
public:
	WindowTwo()
	{
		add_widget(get_widget_factory().create_menu("Edit"));
		add_widget(get_widget_factory().create_button("OK", IconType::ok));
		add_widget(get_widget_factory().create_button("Cancel", IconType::cancel));
	}
};


int main(void)
{
    WindowOne w1;
    w1.display();

    WindowTwo w2;
    w2.display();
}

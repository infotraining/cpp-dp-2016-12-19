#include "circle.hpp"
#include "shape_factories.hpp"

using namespace std;
using namespace Drawing;

namespace
{
	bool is_registered = 
		SingletonShapeFactory::instance()
			.register_creator(Circle::id, &make_unique<Circle>);
}

Circle::Circle(int x, int y, int radius) 
	: ShapeBase(x, y), radius_{radius}
{
}

void Circle::draw() const
{
	cout << "Drawing circle at " << coord() << " with r = " << radius_ << std::endl;
}

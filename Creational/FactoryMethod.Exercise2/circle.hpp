#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

namespace Drawing
{
    class Circle : public ShapeBase
    {    
		int radius_;
    public:
		static constexpr const char* id = "Circle";

		Circle(int x = 0, int y = 0, int radius = 0);

		void set_radius(int r)
		{
			radius_ = r;
		}

		int radius() const
		{
			return radius_;
		}

		void draw() const override;
    };
}

#endif // CIRCLE_H
